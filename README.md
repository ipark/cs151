

CS151. Object-Oriented Design (Fall2018 @SJSU)
==============

Textbook. Object-Oriented Design & Patterns, 3rd edition, by Cay Horstmann
---------

Topics:
--------
1. A Crash Course in Java

2. The Object-Oriented Design Process

3. Guidelines for Class Design

4. Interfaces and Polymorphism

5. Patterns and GUI Programming

6. Inheritance and Abstract Classes

7. The Java Object Model

8. Frameworks

9. Concurrent Programming

10. More Design Patterns (Adapter, Command, Factory, Proxy, Singleton and Visitor)