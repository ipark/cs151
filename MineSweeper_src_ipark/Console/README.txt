TO-DO-LIST
==========
1) Convert to GUI (Swing's JFrame, JPanel, JComponent, MounseListener etc.) 
2) Use enum method 
3) Incorpodate one of patterns from textbook

Current Status
==============
For now console-based, not working on Eclipse's console, just working on the terminal. 
$ javac Game.java; java Game

Five Classes
============
1) Game.java 
(main function in there)

2) Model.java 
(followed Mariia's skeletal data structure as Cell[][])

3) View.java 
(no GUI)

4) Controller.java 
(only replace "switch"-statement from 
https://www.darkcoding.net/software/non-blocking-console-io-is-not-possible/
because we firstly wanted to make a console-based game)

5) Cell.java 
(just minor change as Jing's skeletal code was already sufficient)

