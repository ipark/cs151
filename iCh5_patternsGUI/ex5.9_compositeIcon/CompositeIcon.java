import java.awt.*;
import java.awt.geom.*;
import java.util.*;
import javax.swing.*;

/** OODP 3ed. Ch5
Ex9. Use the composite pattern to define a class CompositeIcon that 
implements the Icon interface type and contains a list of icons. 
Supply a method
                void addIcon(Icon icon, int x, int y)

      CompositeIcon composite = new CompositeIcon(100, 100);
      composite.addIcon(new CarIcon(50), 0, 0);
      composite.addIcon(new MarsIcon(50), 0, 50);
*/
public class CompositeIcon implements Icon
{
   /**
     Constructs a composite icon
     @param width the width of the composite icon
     @param height the height of the composite icon
   */

   private int width;
   private int height;
   private ArrayList<Icon> iconList;
   private ArrayList<Point> pointList;

   public CompositeIcon(int width, int height)
   {
      this.width = width;
      this.height = height;
      iconList = new ArrayList<Icon>();
      pointList = new ArrayList<Point>();
   }

   public int getIconWidth()
   {
      return width;
   }

   public int getIconHeight()
   {
      return height;
   }

   public void paintIcon(Component c, Graphics g, int x, int y)
   {
      int i = 0;
      for (Icon icon : iconList)
      {
         Point dW = pointList.get(i++);
         icon.paintIcon(c, g, x + dW.x, y + dW.y);
      }
   }

   /**
     Adds an icon to this composite icon
     @param icon the icon to add
     @param x x offset of the icon within the composite
     @param y y offset of the icon within the composite
   */
   public void addIcon(Icon icon, int x, int y)
   {
      iconList.add(icon);
      pointList.add(new Point(x, y));
   }
}


