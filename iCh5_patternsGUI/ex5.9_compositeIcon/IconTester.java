import javax.swing.*;

/** OODP 3ed. Ch5
Ex9. Use the composite pattern to define a class CompositeIcon that 
implements the Icon interface type and contains a list of icons. 
Supply a method
                void addIcon(Icon icon, int x, int y)
*/

public class IconTester
{
   public static void main(String[] args)
   {
      CompositeIcon composite = new CompositeIcon(100, 100);
      composite.addIcon(new CarIcon(50), 0, 0);
      composite.addIcon(new MarsIcon(50), 0, 50);
      JOptionPane.showMessageDialog(
         null,
         "Composite Pattern",
         "Message",
         JOptionPane.INFORMATION_MESSAGE,
         composite);
      System.exit(0);
   }
}

