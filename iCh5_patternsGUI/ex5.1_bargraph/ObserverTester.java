import java.util.ArrayList;

/**
 * OODP 3ed Ch5
Ex1. Write a program that contains two frames, 
one with a column of text fields containing numbers, 
and another that draws a bar graph showing the values of the numbers. 

When the user edits one of the numbers, the graph should be redrawn. 
Use the observer pattern. 
Store the data in a model. 
Attach the graph view as a listener. 

When a number is updated, the number view should update the model, 
and the model should tell the graph view that a change has occured. 

As a result, the graph view should repaint itself.
A class for testing an implementation of the Observer pattern.
*/
public class ObserverTester
{
   public static void main(String[] args)
   {
      ArrayList<Integer> data = new ArrayList<Integer>();
      data.add(new Integer(100));
      data.add(new Integer(25));
      data.add(new Integer(80));
      data.add(new Integer(95));

      Model model = new Model(data);
      Controller tframe = new Controller(model);
      Viewer  viewer = new Viewer(model);
      model.attach(viewer);
   }
}
