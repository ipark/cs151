import java.awt.*;
import java.awt.geom.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;

/**
 * Viwer class displaying bar graph upon data change
*/
public class Viewer extends JFrame implements ChangeListener
{
   private ArrayList<Integer> a;
   private Model model;

   private static final int ICON_WIDTH = 400;
   private static final int ICON_HEIGHT = 400;
   // constructor
   public Viewer(Model model)
   {
      this.model = model;
      a = model.getData();

      setLocation(0,200);
      setLayout(new BorderLayout());

      Icon barIcon = new Icon()
      {
         public int getIconWidth() { return ICON_WIDTH; }
         public int getIconHeight() { return ICON_HEIGHT; }
         public void paintIcon(Component c, Graphics g, int x, int y)
         {
            Graphics2D g2 = (Graphics2D) g;
            g2.setColor(Color.blue);
            double max = (a.get(0)).doubleValue();
            for (Integer v: a)
            {
               double val = v.doubleValue();
               if (val > max)
                  max = val;
            }
            double barH = getIconHeight() / a.size();
            int i = 0;
            for (Integer v : a)
            {
               double value = v.doubleValue();
               double barL = getIconWidth() * value / max;
               Rectangle2D.Double bar = new Rectangle2D.Double(0, barH * i, barL, barH);
               i++;
               g2.fill(bar);
            }
         }
      };
      add(new JLabel(barIcon));
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      pack();
      setVisible(true);
   }

   // called when data in model changed
   public void stateChanged(ChangeEvent e)
   {
      a = model.getData();
      repaint();
   }
}
