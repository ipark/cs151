import java.util.ArrayList;
import javax.swing.event.*;

/**
 * Model class has data
 */
public class Model
{
   // instance variables
   ArrayList<Integer> data;
   ArrayList<ChangeListener> listeners;

   // constructor
   public Model(ArrayList<Integer> d)
   {
      data = d;
      listeners = new ArrayList<ChangeListener>();
   }

   // data getter
   public ArrayList<Integer> getData()
   {
      return (ArrayList<Integer>) (data.clone());
   }

   // attach listeners
   public void attach(ChangeListener c)
   {
      listeners.add(c);
   }

   //  update upon data change in a specific position in viwer
   public void update(int position, int value)
   {
      data.set(position, new Integer(value));
      for (ChangeListener cl:listeners)
      {
         cl.stateChanged(new ChangeEvent(this));
      }
   }

}
