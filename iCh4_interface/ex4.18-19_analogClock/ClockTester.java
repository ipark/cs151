import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import javax.swing.Timer;

/** 
 * OODP 3ed Ch4. Exercise 19
 * Continue Exercise 18 by adding a javax.swing.Timer object to your program. 
 * The timer’s action listener should invoke the repaint method once per second. 
/**
 * this program implments an animation that moves a car shape
 *             [CarShape] 
 *             ClockShape
 *                 |
 * [ShapeIcon]-[(i)MovableShape]-[anonymous time listener]
 * ClockIcon       ClockTicShape 
 *   |                            |
 * [(i)Icon]                     [ActionListener]
 *   |                            |
 * [JLabel]                      [Timer]
 */
public class ClockTester
{
  public static void main(String[] args) 
  {
    JFrame frame = new JFrame();

    // ANALOG CLOCK
    //public ClockShape(int x, int y, int width)
    final ClockTicShape shape = new ClockShape(0, 0, 100);
    //public ClockIcon(ClockTicShape shape, int x, int y, int width)
    ClockIcon icon = new ClockIcon(shape, 0, 0, 100); 

    // DIGITAL CLOCK
    final int FIELD_WIDTH = 20; // unit is a number of columns approx. 20-char-width
    final JTextField textField = new JTextField(FIELD_WIDTH);

    final JLabel label = new JLabel(icon);
    frame.setLayout(new FlowLayout());
    frame.add(label);     // ANALOG CLOCK
    frame.add(textField);   // DIGITAL CLOCK

    final int DELAY = 1000; // 1s
    Timer t = new Timer(DELAY, event ->
        {
          label.repaint(); 
          Date now = new Date();
          textField.setText(now.toString()); 
        });
    t.start();

    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setVisible(true);
  }
}

