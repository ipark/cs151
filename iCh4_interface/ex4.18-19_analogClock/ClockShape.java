import java.awt.*;
import java.awt.geom.*;
import java.util.*;
import javax.swing.*;

/**
 * A clock that can be ticked
 */
public class ClockShape implements ClockTicShape
{
  // instance fields
  /**
   @param x the left of the bounding rectangle
   @param y the top of the bounding rectangle
   @param width the width of the bounding rectangle
   */
  private int x;
  private int y;
  private int width;

  public ClockShape(int x, int y, int width)
  {
    this.x = x;
    this.y = y;
    this.width = width;
  }

  // instance method from MoveableShape
  public void draw(Graphics2D g2, int x, int y)
  {
    /*
     (x,y)
       +----width-----+
       |      |       |
       |      |       |
       |    center    |
       |      |       |
       |      |       |
       +----width-----+
      
       center = Point2D.Double(width/2, width/2);
       radius = width/2;
     */
    Point2D.Double centerP = new Point2D.Double(width/2, width/2);
    double radius = width/2;
    Ellipse2D.Double circle = new Ellipse2D.Double(x, y, width, width);
    g2.draw(circle);

    /**
     * get seconds, minutes and hours from GregorianCalendar
     */
     GregorianCalendar now = new GregorianCalendar();
     int seconds = now.get(Calendar.SECOND);
     int minutes = now.get(Calendar.MINUTE);
     int hours = now.get(Calendar.HOUR);

    /**
     * Angle calculation for s, m and h in Radians
     */
    double sRadian = Math.toRadians((360/60) * seconds); // 360degrees = 60 seconds
    double mRadian = Math.toRadians((360/60) * minutes); // 360egrees = 60 minutes
           mRadian += Math.toRadians((360/60)/60 * seconds); // extra move from seconds
    double hRadian = Math.toRadians((360/12) * hours); // 360degrees = 12 hours
           hRadian += Math.toRadians((360/12)/60 * minutes); // extra move from minutes
           hRadian += Math.toRadians((360/12)/60/60 * seconds); // extra move from seconds

    /**
     * (x, y) point location of sHand, mHand and hHand
     * then draw a line from centerP(width/2, width/2)
     */
    final double SECOND_LEN = 0.9 * radius;
    final double MINUTE_LEN = 0.6 * radius;
    final double HOUR_LEN = 0.3 * radius;  
    Point2D.Double sP = new Point2D.Double(
            radius + Math.sin(sRadian) * SECOND_LEN, // Sx
            radius - Math.cos(sRadian) * SECOND_LEN // Sy
            );
    Point2D.Double mP = new Point2D.Double(
            radius + Math.sin(mRadian) * MINUTE_LEN, // Mx
            radius - Math.cos(mRadian) * MINUTE_LEN // My
            );
    Point2D.Double hP = new Point2D.Double(
            radius + Math.sin(hRadian) * HOUR_LEN, // Hx
            radius - Math.cos(hRadian) * HOUR_LEN // Hy
            );
    Line2D.Double sHand = new Line2D.Double(centerP, sP);
    Line2D.Double mHand = new Line2D.Double(centerP, mP);
    Line2D.Double hHand = new Line2D.Double(centerP, hP);
    
    g2.draw(sHand); 
    g2.setStroke(new BasicStroke(2));
    g2.draw(mHand); 
    g2.setStroke(new BasicStroke(4));
    g2.draw(hHand); 
    g2.setColor(Color.black);

    g2.drawString("12",  (float) (x + radius*.9), (float) (y+radius*.25));
    g2.drawString("3",   (float) (x + width*.9), (float) (y + radius));
    g2.drawString("6",  (float) (x + radius), (float) (y + width*0.95));
    g2.drawString("9",  (float) (x + radius*0.1), (float) (y + radius));


  }
}

