import java.awt.*;
/**
 * the shape that can be moved around
 */
//public interface MoveableShape 
public interface ClockTicShape
{
  /**
   * Draws the shape
   * @param g2 the graphics context
   */
  void draw(Graphics2D g2, int x, int y);

}
