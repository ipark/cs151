import java.awt.*;
import java.util.*;
import javax.swing.*;

/**
 * OODP3ed Ch4. Exercise18
 * Write a class ClockIcon that implements the Icon interface type. 
 * Draw an analog clock whose hour, minute, and second hands show the current time. 
 * To get the current hours and minutes, construct an object of type 
 * GregorianCalendar with the default constructor.
 */
public class ClockIcon implements Icon
{
  // instance fields
  private int x;
  private int y;
  private int width;
  private ClockTicShape shape;

  public ClockIcon(ClockTicShape shape, int x, int y, int width)
  {
    this.x = x;
    this.y = y;
    this.width = width;
    this.shape = shape;
  }

  // method from Icon
  public int getIconWidth()
  {
    return width;
  }
  // method from Icon
  public int getIconHeight()
  {
    return width;
  }
  // method from Icon
  public void paintIcon(Component c, Graphics g, int x, int y)
  {
    Graphics2D g2 = (Graphics2D) g;
    shape.draw(g2, x, y);
  }
}



