import javax.swing.*;

/**
 * DrawableTester.java 
 * DrawableIcon(new Car()..) implements Icon
 * Car implements Drawable 
 * Drawable interface { void draw()}
 */ 
public class DrawableTester {
  public static void main(String[] args) {
    JOptionPane.showMessageDialog(
        null, // parent window
        "Drawable Tester", // message
        "Message", // window title
        JOptionPane.INFORMATION_MESSAGE,
        new DrawableIcon(new Car(0, 0, 100), 100, 100));
    System.exit(0);
  }
}
