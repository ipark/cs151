import java.awt.*;
import java.awt.geom.*;
import javax.swing.*;

/**
   An icon that has the shape of a car.
   copied from OODP/Ch4/icon3/CarIcon.java
*/
public class DrawableIcon implements Icon
{
   /**
      Constructs a car of a given width.
      @param width the width of the car
      @param width the width of the car
      @param width the width of the car
   */
   private Drawable drawer;
   private int width;
   private int height;
   public DrawableIcon(Drawable d, int width, int height)
   {
      drawer = d;
      this.width = width;
      this.height = height;
   }
   
   public int getIconWidth()
   {
      return width;
   }

   public int getIconHeight()
   {
      return height;
   }

   public void paintIcon(Component c, Graphics g, int x, int y)
   {
      Graphics2D g2 = (Graphics2D) g;
      drawer.draw(g2);
      ///////////////
   }

}


