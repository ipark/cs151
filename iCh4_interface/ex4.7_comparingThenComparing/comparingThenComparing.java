import java.util.*;

public class comparingThenComparing {
  public static void main(String[] args) {
    ArrayList<Country> countries = new ArrayList<Country>();
    countries.add(new Country("Korea",    38691));
    countries.add(new Country("Russia", 6602000));
    countries.add(new Country("USA",    3797000));
    countries.add(new Country("Canada",    3797000));
    Collections.sort(countries, Comparator.comparing((Country c) -> c.getArea())
        .thenComparing(c -> c.getName()));
    for (Country c: countries)
      System.out.println(c.getName() + "\t" + c.getArea());
  }
}
