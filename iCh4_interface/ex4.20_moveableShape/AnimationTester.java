import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * OODP 3ed. Ch4. Exercise 20
 * Enhance the ShapeIcon class so that it displays multiple moveble shapes. 
 * Then modify the animation program to show a number of moving cars. 
 * Hint: Store all shapes in an array list
 */
/*
   This program implements an animation that moves multiple car shapes.
*/
public class AnimationTester
{
   public static void main(String[] args)
   {
      JFrame frame = new JFrame();

      //final MoveableShape shape = new CarShape(0, 0, CAR_WIDTH);
      final MoveableShape[] shapes = new MoveableShape[4];
      for (int i = 0; i < shapes.length; i++)
        shapes[i] = new CarShape(0 + i * 50, i * 100, CAR_WIDTH);

      ShapeIcon icon = new ShapeIcon(shapes, ICON_WIDTH, ICON_HEIGHT);

      final JLabel label = new JLabel(icon);
      frame.setLayout(new FlowLayout());
      frame.add(label);

      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      frame.pack();
      frame.setVisible(true);

      final int DELAY = 100;
      // Milliseconds between timer ticks
      Timer t = new Timer(DELAY, new
         ActionListener()
         {
            public void actionPerformed(ActionEvent event)
            {
              for (int i = 0; i < shapes.length; i++) {
                shapes[i].translate(1, 0);
                label.repaint();
              }
            }
         });
      t.start();
   }

   private static final int ICON_WIDTH = 400;
   private static final int ICON_HEIGHT = 400;
   private static final int CAR_WIDTH = 100;
}
