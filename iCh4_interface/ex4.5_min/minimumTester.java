import java.util.*;

public class minimumTester {
  public static void main(String[] args) {
    ArrayList a = new ArrayList();
    a.add(new Integer(123456789));
    a.add(new Integer(12));
    a.add(new Integer(1245));
    System.out.println(minimum(a));
  }
  public static Object minimum(ArrayList a) {
    Comparable min = (Comparable) a.get(0);
    for (int i = 0; i < a.size(); i++) {
      Comparable a_i = (Comparable) a.get(i);
      if (a_i.compareTo(min) < 0) // a[i] < min
        min = a_i;
    }
    return min;
  }
}
