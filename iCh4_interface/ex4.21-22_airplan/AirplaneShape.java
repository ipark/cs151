import java.awt.*;
import java.awt.geom.*;
import java.util.*;

/**
 * OODP 3ed. Ch4. 
 * Exercise 21
 * Modify the animation program to show a moving airplane
 *
 * Exercise 22 
 * Modify the animation program to make the moving shape
 * reappear on the left-hand side after it disappears from the frame.
/**
   A car that can be moved around.
*/
public class AirplaneShape implements MoveableShape
{
   /**
      Constructs a car item.
      @param x the left of the bounding rectangle
      @param y the top of the bounding rectangle
      @param width the width of the bounding rectangle
   */
   public AirplaneShape(int x, int y, int width, int ICON_WIDTH)
   {
      this.x = x;
      this.y = y;
      this.width = width;
   }

   public void translate(int dx, int dy, int ICON_WIDTH)
   {
      //x += dx;
      //y += dy;
      if (x > ICON_WIDTH) {
        x = 0;
      }
      x += dx;
      y += dy;
   }

   /**
     (x,y)  dw   2dw  3dw  4dw  5dw  6dw   7dw  w=8dw
        +----+----+----+----+----+----+----+----+
        |    |    |    |    |    |    |    |    |
        +----+----+----+----+----+----+----+----+ dw
        |    |    |    |    |    |    |    |    |
        +----P6---+----+----+----+----+----+----+ 2dw
        |  u1|    | u2 |    |    |    |    |    |
        +----P8---+----+----P4---+----P2---+----+ 3dw
        | /////////////////////////// |   f1    |
        +----+----+----+----+----+----+----+---P1 4dw
        | /////////////////////////// |   f2    |
        +----P9---+----+----P5---+----P3---+----+ 5dw
        |  d1|    | d2 |    |    |    |    |    |
        +----P7---+----+----+----+----+----+----+ 6dw
        |    |    |    |    |    |    |    |    |
        +----+----+----+----+----+----+----+----+ 7dw
        |    |    |    |    |    |    |    |    |
        +----+----+----+----+----+----+----+----+ w=8dw
            dw   2dw  3dw  4dw  5dw  6dw   7dw  w=8dw
    */
   public void draw(Graphics2D g2)
   {
      
      double dw = (double) width/8;
      Rectangle2D.Double body = new Rectangle2D.Double(
          x, y + 3*dw, 6*dw, 2*dw);  

      Point2D.Double P1 = new Point2D.Double(x+width, y+4*dw);
      Point2D.Double P2 = new Point2D.Double(x+6*dw, y+3*dw);
      Point2D.Double P3 = new Point2D.Double(x+6*dw, y+5*dw);
      Point2D.Double P4 = new Point2D.Double(x+4*dw, y+3*dw);
      Point2D.Double P5 = new Point2D.Double(x+4*dw, y+5*dw);
      Point2D.Double P6 = new Point2D.Double(x+dw, y+2*dw);
      Point2D.Double P7 = new Point2D.Double(x+dw, y+6*dw);
      Point2D.Double P8 = new Point2D.Double(x+dw, y+3*dw);
      Point2D.Double P9 = new Point2D.Double(x+dw, y+5*dw);

      Line2D.Double front1 = new Line2D.Double(P1, P2);
      Line2D.Double front2 = new Line2D.Double(P1, P3);
      Line2D.Double up1 = new Line2D.Double(P4, P6);
      Line2D.Double up2 = new Line2D.Double(P6, P8);
      Line2D.Double down1 = new Line2D.Double(P5, P7);
      Line2D.Double down2 = new Line2D.Double(P7, P9);
      
      g2.draw(body);
      g2.draw(front1);
      g2.draw(front2);
      g2.draw(up1);
      g2.draw(up2);
      g2.draw(down1);
      g2.draw(down2);
   }
   private int x;
   private int y;
   private int width;
}
