import java.util.*;
import java.awt.Rectangle;

public class MeasurerTester {
  public static void main(String[] args) {
    // generate 10 random rectagles
    int n = 10;
    Random r = new Random();
    ArrayList<Rectangle> a = new ArrayList<Rectangle>();
    for (int i = 0; i < n; i++)
      a.add(new Rectangle(r.nextInt(9)+1, // to ensure non-zero
                          r.nextInt(9)+1, // to ensure non-zero
                          r.nextInt(9)+1, // to ensure non-zero
                          r.nextInt(9)+1));
    for (Rectangle rect : a)
      System.out.println(rect + " Area(" + rect.width * rect.height + ")");
    System.out.println();
    /////////////////////////////////
    Measurer m = new Measurer() {
      public double measure(Object x) {
        Rectangle r = (Rectangle) x;
        return r.width * r.height;
      }
    };
    /////////////////////////////////
    System.out.println("Rectangle with Maximum Area");
    Rectangle rect = (Rectangle) maximum(a.toArray(), m);
    System.out.println(rect + " Area(" + m.measure(rect) + ")");
    System.exit(0);
  }
                              ////////////
  public static Object maximum(Object[] a, Measurer m) {
      Object maxR = a[0];
      for (int i = 0; i < a.length; i++) {
        if (m.measure(maxR) < m.measure(a[i]))
          maxR = a[i];
      }
      return maxR;
  }
}

/*
import java.util.*;

public interface Measurer {
  double measure(Object x);
}
 */
