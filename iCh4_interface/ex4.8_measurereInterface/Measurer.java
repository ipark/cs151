import java.util.*;

public interface Measurer {
  double measure(Object x);
}
