public class BankAccount implements Comparable<BankAccount> {

  private String name;
  private double balance;

  public BankAccount(String aName, double aBalance) {
    name = aName;
    balance = aBalance;
  }
  public String getName() {
    return name;
  }
  public double getBalance() {
    return balance;
  }
  public double deposit(double amount) {
    return balance += amount;
  }
  public double withdraw(double amount) {
    return balance -= amount;
  }
  public int compareTo(BankAccount other) {
    if (this.getBalance() > other.getBalance()) return 1;
    if (this.getBalance() < other.getBalance()) return -1;
    return 0;
  }
}
