import java.util.*;

public class BankAccountTester {
  public static void main(String[] args) {
    ArrayList<BankAccount> bankaccounts = new ArrayList<>();
    bankaccounts.add(new BankAccount("A",523400.2));
    bankaccounts.add(new BankAccount("B",   300.0));
    bankaccounts.add(new BankAccount("C", 23000.2));
    bankaccounts.add(new BankAccount("D",8883000.2));
    Collections.sort(bankaccounts);
    for (BankAccount b: bankaccounts)
      System.out.println(b.getName() + " : " + b.getBalance());
  }
}
