import java.util.*;

public class maximumTester {
  public static void main(String[] args) {
    ArrayList<String> strings = new ArrayList<String>();
    strings.add(new String("hello"));
    strings.add(new String("hi"));
    strings.add(new String("world!!!!"));
    strings.add(new String("world!"));
    // public class int maxString implements Comparator<String>;
    Comparator<String> comp = new maxString();
    System.out.println(maximum(strings, comp));
  }
  public static String maximum(ArrayList<String> a, Comparator<String> c) {
    Collections.sort(a, c);
    return a.get(a.size()-1);
  }
}
