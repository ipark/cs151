import java.awt.*;
import java.awt.geom.*;
import javax.swing.*;

/**
   An icon that has the shape of the planet CoffeeMug.
*/
public class CoffeeMugIcon implements Icon
{
   /**
      Constructs a CoffeeMug icon of a given size.
      @param aSize the size of the icon
   */
   private int width;
   public CoffeeMugIcon(int aWidth)
   {
      width = aWidth;
   }
   
   public int getIconWidth()
   {
      return width;
   }

   public int getIconHeight()
   {
      return width;
   }

   public void paintIcon(Component c, Graphics g, int x, int y)
   {
      Graphics2D g2 = (Graphics2D) g;
      Rectangle2D.Double mugbody = new Rectangle2D.Double(x, y, width*2/3, width);
      Ellipse2D.Double muggrab = new Ellipse2D.Double(x+width/2, y+width/4, width/3, width/3);

      g2.setColor(Color.red);
      g2.fill(mugbody);
      g2.draw(muggrab);
   }
}
