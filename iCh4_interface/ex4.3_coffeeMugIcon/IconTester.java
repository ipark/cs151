import javax.swing.*;

public class IconTester
{
   public static void main(String[] args)
   {
      JOptionPane.showMessageDialog(
            null, 
            "Hello, Coffee Mug!",
            "Message",
            JOptionPane.INFORMATION_MESSAGE,
            new CoffeeMugIcon(50));
      System.exit(0);
   }
}

