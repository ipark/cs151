import java.util.*;

public class FilterTester {
  public static void main(String[] args) {
    ArrayList<String> a = new ArrayList<String>();
    a.add("hi");
    a.add("hello");
    a.add("world");
    a.add("bye");
    a.add("$");
     
    //for (String s : a)
    //  System.out.println(s);
    ////////////////////////
    Filter f = new Filter() {
      public boolean accept(String x) {
        if (x.length() >= 3)
          return true; 
        return false;
      } 
    };
    ////////////////////////
    String[] b = filter(a.toArray(new String[a.size()]), f);

    System.out.println("before\tafter\n============");
    for (int i = 0; i < b.length; i++) {
      System.out.print(a.get(i) + "\t");
      System.out.println(b[i]);
    }
  } // EO-main()

  public static String[] filter(String[] a, Filter f) {
    for (int i = 0; i < a.length; i++) {
      if (!f.accept(a[i]))
        a[i] = " ";
    }
    return a;
  } // EO-filter
} // EO-class
/*
 * Filter.java
public interface Filter { 
  boolean accept(String x); 
}
*/


