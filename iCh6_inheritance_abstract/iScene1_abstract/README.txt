iScene1
├─1 SceneEditor.java (main; FJame; JButton)
├─2 SceneShape.java (interface)
├─3 SelectableShape.java (concrete implementation)
├─4 SceneComponent.java (extends JComponent; for mouse listener attachment)
├─5 CarShape.java 
├─6 HouseShape.java

6.3 Abstract Classes

├── SceneShape.java (interface)
  public interface SceneShape
  {
    void setSelected(boolean b);
    boolean isSelected();
    void draw(Graphics2D g2);
    void drawSelection(Graphics2D g2);
    void translate(int dx, int dy);
    boolean contains(Point2D aPoint);
  }

////INSTEAD OF THESES
├── HouseShape.java
  public class HouseShape implementation SceneShape   {
    public void setSelected(boolean b) { selected = b;}
    public booelan isSelected() { return selected; }
    private booelan selected;                         }
├── CarShape.java 
  public class CarShape implementation SceneShape     {
    public void setSelected(boolean b) { selected = b;}
    public booelan isSelected() { return selected; }
    private booelan selected;                         }

////FACTOR OUT INTO
├── SelectableShape.java (concrete implementation)
  public ABSTRACT class SelectableShape implementation SceneShape {
    public void setSelected(boolean b) { selected = b;}
    public booelan isSelected() { return selected; }
    private booelan selected;                           }
├── HouseShape.java
  public class HouseShape extends SelectableShape {...}
├── CarShape.java 
  public class CarShape extends SelectableShape {...}

