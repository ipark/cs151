/**
 * these two methods are implemented in the 
 * class SelectableShape via abstract
 *
 * public abstract class SelectableShape implements SceneShape
void setSelected(boolean b);
boolean isSelected();
 */

public abstract class SelectableShape implements SceneShape
{
  private boolean setSelected;
  public void setSelected(boolean b) { setSelected = b; }
  public boolean isSelected() { return setSelected; }
}
