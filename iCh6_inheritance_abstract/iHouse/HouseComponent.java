import java.awt.*;
import java.awt.event.*; // mouse listener
import java.awt.geom.*; // graphics
import javax.swing.*;
import java.util.*;

/**
 * inherit JComponent
 * add JComponent.Listners
 * extra method of paintComonent()
 */
public class HouseComponent extends JComponent
{
  /**
   * private fields
   */
  private HouseShape house;
  private Point mousePoint;
  /**
   * JComponent.Listner()
   * 1. mouseListener 
   * 2. mouseMitionListener
   */

  public HouseComponent() 
  {
    /**
     * HouseShape constructor has arguments:
     * public HouseShape(int x, int y, int width)
     */
    // JComponent
    house = new HouseShape(40, 40, 80);
    // JComponent.Listener
    // Listener1
    //
    addMouseListener(new MouseAdapter() 
      {
        public void mousePressed(MouseEvent event)
        {
          mousePoint = event.getPoint();
          if (!house.contains(mousePoint))
            mousePoint = null;
        } // EO-mousePressed
      }); 
    // Listener2
    addMouseMotionListener(new MouseMotionAdapter() 
      {
        public void mouseDragged(MouseEvent event)
        {
          if (mousePoint == null)
            return;
          Point lastMousePoint = mousePoint;
          mousePoint = event.getPoint();
          double dx = mousePoint.getX() - lastMousePoint.getX();
          double dy = mousePoint.getY() - lastMousePoint.getY();
          house.translate((int) dx, (int) dy);
          repaint(); // JComponent's method
        } // EO-mouseDragged
      });
  } // EO-constructor-HouseComponent

  // sub-class specific method
  public void paintComponent(Graphics g)
  {
    Graphics2D g2 = (Graphics2D) g;
    house.draw(g2); 
  }
}
