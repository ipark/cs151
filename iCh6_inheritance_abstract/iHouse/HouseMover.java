import java.awt.*;
import java.awt.event.*; // mouse listener
import java.awt.geom.*; // graphics
import javax.swing.*;

public class HouseMover 
{
  // priviate fileds
  private static final int FRAME_WIDTH = 400;
  private static final int FRAME_HEIGHT = 400;

  public static void main(String[] args) 
  {
    /**
     * A. Window - JFrame
     */
    JFrame frame = new JFrame();

    /**
     * B. Component - HouseComponent extends JComponent
     * C. frame.add(Jcomponent);
     */
    HouseComponent house = new HouseComponent();
    frame.add(house);

    /** 
     * D. JFrame 3 rubrics
     * 1 frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
     * 2 frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
     * 3 frame.setVisible(ture)
     */
    frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setVisible(true);
  } // EO-main
}
