iCar/
├── CarMover.java (main; JFrame)
├── CarComponent.java (extends JComponent; mouseListner attached)
├── CarShape.java (no touch)

6.2 Graphics Programming with Inheritance

6.2.1) designing subclasses of JComponent class
  super-class: JComponent
  sub-class: CarComponent

6.2.2) listener interface types and adapter classes

├── CarComponent.java (extends JComponent; mouseListner attached)
  public interface MouseListener
  {
    void mouseClicked(MouseEvent e);
    void mousePressed(MouseEvent e);
    void mouseReleased(MouseEvent e);
    void mouseEntered(MouseEvent e);
    void mouseExited(MouseEvent e);
  }
  public interface MouseMotionListener
  {
    void mouseMoved(MouseEvent e);
    void mouseDragged(MouseEvent e);
  }


