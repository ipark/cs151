Exercise 6.13. 
Reorganize the code for the scene editor as follows:
1) Define a class SceneFrame that extends the JFrame class. 
2) Its constructor should set up the scene component and the buttons. 
3) The main method of the SceneEditor class should merely construct the SceneFrame and show it.

Solution
========
Modifications from scene1/ source codes

scene1_modified/
├── CarShape.java 
├── HouseShape.java
├── SceneComponent.java
├── SceneEditor.java ******* modified
├── SceneFrame.java ********* new file
├── SceneShape.java
└── SelectableShape.java


├── SceneEditor.java ******* modified

1) modified
     //Frame frame = new JFrame();
     SceneFrame sframe = new SceneFrame();
     sframe.setVisible(true);

2) delete the rest of JFrame related code, 
   move them to a separate file SceneFrame.java

├── SceneFrame.java ********* new file

1) public class SceneFrame extends JFrame
thus, no prefix associated with JFrame such as "frame."
      //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

///////////////////////////////

Exercise 6.14. 
Add more items to the scene editor (such as trucks, stop signs, and so on).

Solution
========

scene1_modified/
├── CarShape.java
├── HouseShape.java
├── SceneComponent.java
├── SceneEditor.java
├── SceneFrame.java
├── SceneShape.java
├── SelectableShape.java
└── TruckShape.java  ******* (Added)

public class TruckShape extends SelectableShape
   public void draw(Graphics2D g2)
   {
    ...
      Rectangle2D.Double top
         = new Rectangle2D.Double(x,y, width/3, width/3);
    ...
   }
  
   public void drawSelection(Graphics2D g2)
   {
      Rectangle2D.Double top
         = new Rectangle2D.Double(x,y, width/3, width/3);
      g2.fill(top);
   }
