import java.awt.*;
import java.awt.event.*; // mouse listener
import java.awt.geom.*; // graphics
import javax.swing.*;
import java.util.*;

/**
 * inherit JComponent
 * add JComponent.Listners
 * extra method of paintComonent()
 */
public class SceneComponent extends JComponent
{
  /**
   * private fields
   */
  //private SceneShape car;
  /**
   * array of shapes
   */
  private ArrayList<SceneShape> shapes;
  private Point mousePoint;

  /**
   * JComponent.Listner()
   * 1. mouseListener 
   * 2. mouseMitionListener
   */

  public SceneComponent() 
  {
    /**
     * SceneShape constructor has arguments:
     * public SceneShape(int x, int y, int width)
     */
    // JComponent
    //car = new SceneShape(20, 20, 50) ;
    /**
     * array of shapes
     */
    shapes = new ArrayList<SceneShape>();

    // JComponent.Listener
    // Listener1
    //
    addMouseListener(new MouseAdapter() 
      {
        public void mousePressed(MouseEvent event)
        {
          mousePoint = event.getPoint();
          //if (!car.contains(mousePoint))
          //  mousePoint = null;
          /**
           * array of shapes
           */
          for (SceneShape s : shapes)
          {
            if (s.contains(mousePoint))
              s.setSelected(!s.isSelected());
          }
          repaint();
        } // EO-mousePressed
      }); 
    // Listener2
    addMouseMotionListener(new MouseMotionAdapter() 
      {
        public void mouseDragged(MouseEvent event)
        {
          if (mousePoint == null)
            return;
          Point lastMousePoint = mousePoint;
          mousePoint = event.getPoint();
          /**
           * array of shapes
           */
          for (SceneShape s : shapes)
          {
            if (s.isSelected())
            {
              double dx = mousePoint.getX() - lastMousePoint.getX();
              double dy = mousePoint.getY() - lastMousePoint.getY();
              //car.translate((int) dx, (int) dy);
              s.translate((int) dx, (int) dy);
            }
          }
          repaint(); // JComponent's method
        } // EO-mouseDragged
      });
  } // EO-constructor-SceneComponent

  // sub-class specific method
  public void paintComponent(Graphics g)
  {
    Graphics2D g2 = (Graphics2D) g;
    //car.draw(g2); 
    /**
     * array of shapes
     */
    for (SceneShape s : shapes)
    {
      s.draw(g2); 
      if (s.isSelected())
        s.drawSelection(g2);
    }
  }

  //////////////////////////////////
  ////////NEW METHODS///////////////
  //////////////////////////////////
  public void add(SceneShape s)
  {
    shapes.add(s);
    repaint();
  }
  public void removeSelected()
  {
    for (int i = shapes.size() - 1; i >= 0; i--)
    {
      SceneShape s = shapes.get(i);
      if (s.isSelected())
        shapes.remove(i);
    }
    repaint();
  }
  //////////////////////////////////
  //////////////////////////////////
}
