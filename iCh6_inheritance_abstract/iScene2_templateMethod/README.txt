6.4 Template Method Pattern

CHANGES in iScene2_templateMethod/ from iScene1_abstract/ 

iScene2_templateMethod/
=======================          
1) ADDITION in SelectableShape.java 
import java.awt.*;
import java.awt.geom.*;
  public void drawSelection(Graphics2D g2)
  {
    translate(1, 1);
    draw(g2);
    translate(1, 1);
    draw(g2);
    translate(-2, -2);
  }
2) DELETION in  HouseShape.java
   /**
    * deprecated 
    * replcated with template method pattern in SelectableShape class
    *
   public void drawSelection(Graphics2D g2)
   {
      Rectangle2D.Double base 
         = new Rectangle2D.Double(x, y + width, width, width);
      g2.fill(base);
   }
   */
3) DELETION in  CarShape.java
   /**
    * deprecated 
    * replcated with template method pattern in SelectableShape class
    *
   public void drawSelection(Graphics2D g2)
   {
      Rectangle2D.Double body
         = new Rectangle2D.Double(x, y + width / 6, 
            width - 1, width / 6);
      g2.fill(body);
   }
    */
