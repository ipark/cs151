import java.awt.*;
import java.awt.geom.*;
/**
 * these two methods are implemented in the 
 * class SelectableShape via abstract
 *
 * public abstract class SelectableShape implements SceneShape
void setSelected(boolean b);
boolean isSelected();
 */

public abstract class SelectableShape implements SceneShape
{
  private boolean setSelected;
  public void setSelected(boolean b) { setSelected = b; }
  public boolean isSelected() { return setSelected; }

  /** 
   * primitiveOps
   * for the Template Method Pattern
  CarShape.java:void translate(int dx, int dy);
  HouseShape.java:void translate(int dx, int dy);
  CarShape.java:void draw(Graphics2D g2);
  HouseShape.java:void draw(Graphics2D g2);

  and deprecated drawSelection(Graphics2D g2)
  in CarShape.java and HouseShape.java
   */
  public void drawSelection(Graphics2D g2)
  {
    translate(1, 1);
    draw(g2);
    translate(1, 1);
    draw(g2);
    translate(-2, -2);
  }
}

