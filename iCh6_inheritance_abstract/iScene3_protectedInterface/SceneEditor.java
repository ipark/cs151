import java.awt.*;
import java.awt.event.*; // mouse listener
import java.awt.geom.*; // graphics
import javax.swing.*;

public class SceneEditor
{
  // priviate fileds
  private static final int FRAME_WIDTH = 400;
  private static final int FRAME_HEIGHT = 400;

  public static void main(String[] args) 
  {
    /**********************************************
     * A. Window - JFrame
     *********************************************/
    JFrame frame = new JFrame();

    /**********************************************
     * B. Component - SceneCompont entxtends JComponent
     * C. frame.add(Jcomponent);
     *********************************************/
    //CarComponent car = new CarComponent();
    //frame.add(car);
    /**
     * array of shapes
     */
    final SceneComponent scene = new SceneComponent();
    frame.add(scene, BorderLayout.CENTER);

    // houseButton
    JButton houseButton = new JButton("House");
    houseButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent event)
      {
        scene.add(new HouseShape(30, 30, 60));
      }   
    });
    // carButton
    JButton carButton = new JButton("Car");
    carButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent event)
      {
        scene.add(new CarShape(30, 30, 50));
      }   
    });
    // removeButton
    JButton removeButton = new JButton("Remove");
    removeButton.addActionListener(new ActionListener()
    {
      public void actionPerformed(ActionEvent event)
      {
        scene.removeSelected();
      }   
    });

    JPanel buttons = new JPanel();
    buttons.add(houseButton);
    buttons.add(carButton);
    buttons.add(removeButton);
    frame.add(buttons, BorderLayout.NORTH);

    /**********************************************
     * D. JFrame 3 rubrics
     * 1 frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
     * 2 frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
     * 3 frame.setVisible(ture)
     *********************************************/
    frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setVisible(true);
  } // EO-main
}
