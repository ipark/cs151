import java.awt.*;
import java.awt.geom.*;

public interface SceneShape
{
  /**
   * these two methods are implemented in the 
   * class SelectableShape via abstract
   *
   * public abstract class SelectableShape implements SceneShape
   */
  void setSelected(boolean b);
  boolean isSelected();

  /**
   * these four methods are implemented in both 
   * CarShape and HouseShsape classes via inheritance from SelectableShape
   * 
   * public class HouseShape extends SelectableShape
   * public class CarShape extends SelectableShape 
   */
  void draw(Graphics2D g2);
  void drawSelection(Graphics2D g2);
  void translate(int dx, int dy);
  boolean contains(Point2D aPoint);
}
