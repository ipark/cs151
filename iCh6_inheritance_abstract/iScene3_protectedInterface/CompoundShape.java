import java.awt.*;
import java.awt.geom.*;

/**
 * TAKE all methods from CarShape/HouseShape
 * ADD protected Add method
 * and make an abstract class
 */
public abstract class CompoundShape extends SelectableShape
{
  private GeneralPath path;

  public CompoundShape()
  {
    path = new GeneralPath();
  }

  protected void add(Shape s)
  {
    path.append(s, false);
  }

  public void draw(Graphics2D g2)
  {
    g2.draw(path);
  }
  
  public boolean contains(Point2D aPoint)
  {
    return path.contains(aPoint);
  }

  public void translate(int dx, int dy)
  {
    path.transform(AffineTransform.getTranslateInstance(dx, dy));
  }
}
