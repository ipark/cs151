6.5 Protected Inheritances


CHANGES in iScene3_protectedInterface/ from iScene2_templateMethod/

iScene3_protectedInterface/
==========================
1) 7th java file; Add an abstract class CompoundShape.java 
/**
 * TAKE all methods from CarShape/HouseShape
 * ADD protected Add method
 * and make an abstract class
 */
public abstract class CompoundShape extends SelectableShape

2) Now, HouseShape, CarShape extends CompoundShape 

HouseShape.java and CarShape.java
/**
 * ONLY leave HouseShape components inside a constructor
 * all methods move to abstract class in CompoundShape.java
 *
 */
//public class HouseShape
//public class HouseShape extends SelectableShape // iScene2_templateMethod
public class HouseShape extends CompoundShape // iScene3_protectedInterface


3) no more g2.draw(), but just add() inside a constructor of
HouseShape.java and CarShape.java
/**
 * change g2.draw to add
 * since HouseShape extends CompoundShape
g2.draw(base);
g2.draw(roofLeft);
g2.draw(roofRight);
 */
add(base);
add(roofLeft);
add(roofRight);
