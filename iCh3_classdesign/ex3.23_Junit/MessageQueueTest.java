import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
/**
 * OODP 3ed 
  Exercise 3.23. 
  =============
  Design a test class to test the MessageQueue class with JUnit.
 
  javac -cp \
  .:Junit_jars/junit-jupiter-api-5.3.1.jar:Junit_jars/apiguardian-api-1.0.0.jar \
  MessageQueueTest.java 

  java -jar Junit_jars/junit-platform-console-standalone-1.3.1.jar \
  -cp . -c MessageQueueTest
*/
public class MessageQueueTest
{

    @Test
    public void testIsFull()
    {
        MessageQueue messageQueue = new MessageQueue(1);
        messageQueue.add(new Message("one"));
        assertTrue(messageQueue.isFull());
    }


   @Test public void testIsEmpty()
   {
     int capacity = 5;
     MessageQueue m = new MessageQueue(2); 
     for (int i = 0; i < m.getCapacity(); i++)
        m.add(new Message("message " + i+1));
     for (int i = 0; i < m.getCapacity(); i++)
        m.remove();
     assertTrue(m.isEmpty());
   }

   @Test public void testAdd()
   {
      MessageQueue m = new MessageQueue(2); 
      m.add(new Message("one"));
      m.add(new Message("two"));
      assertEquals(m.size(), 2);
   }

}
