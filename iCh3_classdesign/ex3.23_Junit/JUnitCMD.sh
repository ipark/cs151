#!/bin/bash
javac -cp \
.:Junit_jars/junit-jupiter-api-5.3.1.jar:Junit_jars/apiguardian-api-1.0.0.jar \
MessageQueueTest.java 

java -jar Junit_jars/junit-platform-console-standalone-1.3.1.jar \
-cp . -c MessageQueueTest
