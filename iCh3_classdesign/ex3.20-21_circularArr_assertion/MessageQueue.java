import java.util.ArrayList;

/**
 * OODP 3ed, Ch2
   A first-in, first-out collection of messages. This
   implementation is not very efficient. We will consider
   a more efficient implementation in chapter 3.

  * OODP 3ed, Ch3
  Exercise 3.20. Improve the circular array implementation 
  of the bounded queue by growing the elements array when 
  the queue is full.
  
  Exercise 3.21. Add assertions to check all preconditions 
  of the methods of the bounded queue implementation.
*/
public class MessageQueue
{
   /**
      New implementation for MessageQueue using
      Circular Array of Messages
      Message[] queue;
      by using head, tail and count 
      i.e. head = head % size
      i.e. tail = tail % size
      remove message from head; count--
      add message to tail; count++
   */
   //private ArrayList<Message> queue;
   private Message[] queue;
   private int head; 
   private int tail; 
   private int count; 

   // constructor
   public MessageQueue(int capacity)
   {
     queue = new Message[capacity];
     head = 0;
     tail = 0;
     count = 0;
   }

   /**
      Remove message at head.
      @return message that has been removed from the queue
      @precondition !isEmpty()
   */
   public Message remove()
   {
      assert !isEmpty() : "empty array can't operate remove()";
      Message m = queue[head];
      head = (head + 1) % queue.length; // circular array index
      count--;
      return m; 
   }

   /**
      Append message at tail.
      @param newMessage the message to be appended
      @precondition !isFull()
   */
   public void add(Message newMessage)
   {
     assert !isFull() : "array is full, no space to add(message)";
     queue[tail] = newMessage;
     tail = (tail + 1) % queue.length;
     count++;
   }

   public int getCapacity()
   {
     return queue.length;
   }

   /**
      Get the total number of messages in the queue.
      @return the total number of messages in the queue
   */
   public int size()
   {
      return count;
   }

   /**
    * Get boolean whether Message array is full
    * @return boolean true/false
    */
   public boolean isFull()
   {
     return (count == queue.length);
   }

   /**
    * Get boolean whether Message array is empty
    * @return boolean true/false
    */
   public boolean isEmpty()
   {
     return (count == 0);
   }

   /**
      Get message at head.
      @return message that is at the head of the queue, or null
      if the queue is empty
   */
   public Message peek()
   {
      if (queue.length == 0) return null;
      else return queue[head];
   }
}
