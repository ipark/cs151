public class MessageQueueTester 
{
  public static void main(String[] args) 
  {
    System.out.println("$java -ea MessageQueueTester");

    /*
    System.out.println("Assertion of add(new Message) on the full Message array");
    MessageQueue mq = new MessageQueue(2);
    mq.add(new Message("mesage1"));
    mq.add(new Message("mesage2"));
    mq.add(new Message("mesage3"));
    */

    System.out.println("Assertion of remove() call on the empty Message array");
    MessageQueue mq = new MessageQueue(2);
    mq.remove();
    mq.remove();
    mq.remove();
  }
}
